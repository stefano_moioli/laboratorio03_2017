package it.unimi.di.sweng.lab03;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class LinkedListTest {

	@Rule
    public Timeout globalTimeout = Timeout.seconds(2);

	private IntegerList list;
	
	@Test
	public void noParametersConstructor(){
		list = new IntegerList();
		assertThat(list.toString()).isEqualTo("[]");
	}
	
	@Test
	public void addLastTest(){
		list = new IntegerList();
		list.addLast(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addLast(3);
		assertThat(list.toString()).isEqualTo("[1 3]");
	}
	
	@Test
	public void stringConstructorTest(){
		assertThat(new IntegerList("").toString()).isEqualTo("[]");
		assertThat(new IntegerList("1").toString()).isEqualTo("[1]");
		assertThat(new IntegerList("1 2 3").toString()).isEqualTo("[1 2 3]");
	}
	
	@Test
	public void stringConstructorExceptionTest(){
		try{
			list = new IntegerList("1 2 aaa");
			failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
		} catch(IllegalArgumentException e){}
		
		
	}
	
	@Test
	public void addFirstTest(){
		list = new IntegerList();
		list.addFirst(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addFirst(3);
		assertThat(list.toString()).isEqualTo("[3 1]");
	}

	@Test
	public void removeFirstTest(){
		
		list = new IntegerList("1 2");
		assertThat(list.removeFirst()).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[2]");
		
		list = new IntegerList("2");
		assertThat(list.removeFirst()).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[]");
		
		list = new IntegerList("");
		assertThat(list.removeFirst()).isEqualTo(false);
		assertThat(list.toString()).isEqualTo("[]");
	}
	
	@Test
	public void removeLastTest(){
		list = new IntegerList("7 10");
		assertThat(list.removeLast()).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[7]");
		
		list = new IntegerList("7");
		assertThat(list.removeLast()).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[]");
		
		list = new IntegerList("");
		assertThat(list.removeLast()).isEqualTo(false);
		assertThat(list.toString()).isEqualTo("[]");
	}
}
