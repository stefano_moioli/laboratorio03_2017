package it.unimi.di.sweng.lab03;

public class IntegerNode {
	private Integer value = null;
	private IntegerNode previous = null;
	private IntegerNode next = null;
	
	public IntegerNode(int i){
		value = i;
	}
	
	public IntegerNode previous() {
		return previous;
	}
	
	public IntegerNode next() {
		return next;
	}

	public void setPrevious(IntegerNode integerNode){
		previous = integerNode;
	}
	
	public void setNext(IntegerNode integerNode) {
		next = integerNode;
	}

	public int getValue() {
		return value;
	}
}
