package it.unimi.di.sweng.lab03;

import org.junit.runners.ParentRunner;

public class IntegerList {
	
	private IntegerNode head = null;
	private IntegerNode tail = null;
	
	public IntegerList(){
	}
	
	public IntegerList(String init) {
		if(init.isEmpty())
			return;
		
		String items[] = init.split(" ");
		for(String item: items){
			int value = Integer.parseInt(item);
			addLast(value);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("[");
		IntegerNode currentNode;
		int i;
		for(currentNode = head, i=0;
			currentNode != null;
			i++, currentNode = currentNode.next()
		){
			if(i > 0)
				sb.append(' ');
			sb.append(currentNode.getValue());
		}
		sb.append(']');
		return sb.toString();
	}

	private boolean initialize(int i){
		if(head == null){
			head = tail = new IntegerNode(i);
			return true;
		}
		return false;
	}
	
	public void addLast(int i) {
		if(!initialize(i)){
			IntegerNode node = new IntegerNode(i);
			node.setPrevious(tail);
			tail.setNext(node);
			tail = node;
		}
	}

	public void addFirst(int i) {
		if(!initialize(i)){
			IntegerNode node = new IntegerNode(i);
			node.setNext(head);
			head.setPrevious(node);
			head = node;
		}
	}

	public boolean removeFirst() {
		if (head == null){
			return false;
		} else {
			head = head.next();
			return true;
		}
		
	}
	
	public boolean removeLast(){
		if(tail == null){
			return false;
		} else if(head.next() == null){
			head = tail = null;
			return true;
		} else {
			IntegerNode previous = tail.previous();
			previous.setNext(null);
			tail = previous;
			return true;
		}
	}
}
